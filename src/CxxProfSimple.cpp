#include "cxxprof_simple/CxxProfSimple.h"
#include "cxxprof_simple/Activity.h"
#include "cxxprof_simple/Mark.h"
#include "cxxprof_simple/Plot.h"

#include <algorithm>
#include <exception>
#include <functional>
#include <string>

namespace CxxProf
{
    const std::string PLUGINDIR = ".";

    CxxProfSimple& CxxProfSimple::getCxxProf()
    {
        static CxxProfSimple instance_;
        return instance_;
    }

    CxxProfSimple::CxxProfSimple() :
        actCounter_(1),
        profilingStart_(std::chrono::high_resolution_clock::now())
    {
        objectBuffer_.Info.Starttime = 0;
        objectBuffer_.Info.Name = "App";
    }

    CxxProfSimple::~CxxProfSimple()
    {}

    void CxxProfSimple::initialize()
    {
        dataStorage_.initialize();
    }

    std::unique_ptr<IActivity> CxxProfSimple::createActivity(const std::string& name)
    {
        unsigned int newActId = actCounter_++;

        //get the current Thread Id, create it if the Thread has no ID yet
        unsigned int threadId = checkThreads();
        
        std::unique_ptr<Activity> newAct(new Activity(name,
            newActId,
            threadId,
            0, //we're setting the parentId later
            profilingStart_,
            std::bind(&CxxProfSimple::addResult, this, std::placeholders::_1)));

        //check if this is the newest Activity
        //set the ParentId then
        if (activeActivity_.empty())
        {
            newAct->setParentId(0);
        }
        else
        {
            newAct->setParentId(activeActivity_.top());
        }

        //make the new activity the active one
        newAct->start();
        activeActivity_.push(newActId);

        return newAct;
    }

    void CxxProfSimple::addMark(const std::string& name)
    {
        Mark newMark;
        newMark.Name = name;
        newMark.Timestamp = (std::chrono::high_resolution_clock::now() - profilingStart_).count();

        std::unique_lock<std::mutex> lock(bufferMutex_); //protect the objectBuffer
        objectBuffer_.Marks.push_back(newMark);
    }

    void CxxProfSimple::addPlotValue(const std::string& name, double value)
    {
        Plot newPlot;
        newPlot.Name = name;
        newPlot.Timestamp = (std::chrono::high_resolution_clock::now() - profilingStart_).count();
        newPlot.Value = value;

        std::unique_lock<std::mutex> lock(bufferMutex_); //protect the objectBuffer
        objectBuffer_.Plots.push_back(newPlot);
    }

    void CxxProfSimple::setProcessAlias(const std::string& name)
    {
        //TODO: It should be checked if there has already been an info sent, if that is the case we shouldn't
        //      change the name anymore.
        //      This can be checked by seeing if addMark, addPlot or addActivity has been called before
        objectBuffer_.Info.Name = name;
    }
    void CxxProfSimple::setThreadAlias(const std::string& name)
    {
        //TODO: Ensure that the ThreadAlias is only set once, it would get quite complicated
        //      if it changes while we're collecting data.

        //first ensure that the current Thread is known
        unsigned int threadId = checkThreads();

        //then set the alias to the given value
        objectBuffer_.Info.ThreadAliases[threadId] = name;
    }

    void CxxProfSimple::shutdown()
    {
        //TODO: We need to wait until all activities are ended before sending the objects...
        //      How do we know about that?
        //      Call for each open activity:
        //          Activity->shutdown();

        //Store the objects
        storeObjects();
    }
    
    void CxxProfSimple::addResult(const ActivityResult& result)
    {
        //the mutex protects from different activities calling this callback at once
        std::unique_lock<std::mutex> cbLock(callbackMutex_);

        activeActivity_.pop();
        
        std::unique_lock<std::mutex> bufferLock(bufferMutex_); //protect the objectBuffer
        objectBuffer_.ActivityResults.push_back(result);
    }

    unsigned int CxxProfSimple::checkThreads()
    {
        unsigned int threadId = 0;
        
        //check if the ThreadId already exists
        std::vector<std::thread::id>::iterator resultIter = std::find(knownThreads_.begin(), knownThreads_.end(), std::this_thread::get_id());
        if (resultIter == knownThreads_.end()) //if not, add it to the list of known IDs
        {
            knownThreads_.push_back(std::this_thread::get_id());
            threadId = knownThreads_.size() - 1;

            //also add it to the ThreadIdentifiers
            objectBuffer_.Info.ThreadAliases[threadId] = "Thread #" + std::to_string(threadId);
        }
        else //if the Id exists, add it as threadId
        {
            //NOTE: See http://stackoverflow.com/questions/2152986/best-way-to-get-the-index-of-an-iterator
            //      on why we do not use std::distance here
            threadId = resultIter - knownThreads_.begin();
        }

        return threadId;
    }
    
    void CxxProfSimple::storeObjects()
    {
        std::unique_lock<std::mutex> lock(bufferMutex_); //protect the objectBuffer
        dataStorage_.storeResult(objectBuffer_);
    }

} //namespace CxxProf
