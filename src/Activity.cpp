
#include "cxxprof_simple/Activity.h"

#include <chrono>
#include <iostream>

namespace CxxProf
{

    Activity::Activity() :
        name_(""),
        threadId_(0),
        actId_(0),
        parentId_(0),
        profilingStart_(std::chrono::high_resolution_clock::now())
    {}

    Activity::Activity(const std::string& name, unsigned int actId, unsigned int threadId, unsigned int parentId, const std::chrono::high_resolution_clock::time_point& profilingStart, ResultCallback callback) :
        name_(name),
        threadId_(threadId),
        actId_(actId),
        parentId_(parentId),
        callback_(callback),
        profilingStart_(profilingStart)
    {}

    Activity::~Activity()
    {
        executeCallback();
    }

    void Activity::setName(const std::string& name)
    {
        name_ = name;
    }
    void Activity::setThreadId(unsigned int threadId)
    {
        threadId_ = threadId;
    }
    void Activity::setActId(unsigned int actId)
    {
        actId_ = actId;
    }
    void Activity::setParentId(unsigned int parentId)
    {
        parentId_ = parentId;
    }

    void Activity::setResultCallback(ResultCallback callback)
    {
        callback_ = callback;
    }

    void Activity::setProfilingStart(const std::chrono::high_resolution_clock::time_point& profilingStart)
    {
        profilingStart_ = profilingStart;
    }

    void Activity::start()
    {
        starttime_ = getCurrentTime();
    }

    void Activity::shutdown()
    {
        executeCallback();
        callback_ = 0;
    }

    void Activity::executeCallback()
    {
        if (callback_)
        {
            ActivityResult result;
            result.Name = name_;
            result.ThreadId = threadId_;
            result.ActId = actId_;
            result.ParentId = parentId_;
            result.Starttime = starttime_;
            result.Stoptime = getCurrentTime();

            callback_(result);
        }
    }

    std::int64_t Activity::getCurrentTime()
    {
        std::chrono::high_resolution_clock::duration diff = std::chrono::high_resolution_clock::now() - profilingStart_;
        return diff.count();
    }
}
