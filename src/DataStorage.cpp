
#include "cxxprof_simple/DataStorage.h"

#include <ctime>
#include <iostream>
#include <stdlib.h> // needed for exit()

namespace CxxProf
{
    DataStorage::DataStorage()
    {}

    DataStorage::~DataStorage()
    {}
    
    void DataStorage::initialize()
    {
        //create a new database for each session
        //NOTE: db init is always needed, we're creating a new file every time this function is called...
        std::string dbName = "session_" + getCurrentDateStr() + ".db";
        database_.reset(new SQLite::Database(dbName, SQLite::OPEN_READWRITE|SQLite::OPEN_CREATE));
        initDatabase(dbName);
    }

    std::string DataStorage::getCurrentDateStr()
    {
        char buff[20];
        time_t now = time(NULL);
        strftime(buff, 20, "%d%b%Y_%H%M%S", localtime(&now));
        
        return buff;
    }

    void DataStorage::initDatabase(const std::string& filename)
    {
        //create a deferred transaction for the following commands
        //Simple explanation: If we do not wrap everything into a transaction here the performance would be VERY bad
        SQLite::Transaction transaction(*database_);

        try
        {
            //Create table for the AppInfo
            //We're making Name UNIQUE here because there shouldn't be more than 1 application with the same name
            //This makes it easier when we're adding the applicationInfo to the DB later
            SQLite::Statement initAppInfoCommand(*database_,
                "CREATE TABLE appinfo ( "
                "   AppId INTEGER PRIMARY KEY AUTOINCREMENT, "
                "   Name TEXT UNIQUE, "
                "   Starttime INTEGER"
                ")");

            //Create table for the ThreadInfo
            //We do not need to autoincrement here as the Id is unique on userside
            SQLite::Statement initThreadInfoCommand(*database_,
                "CREATE TABLE threadinfo ( "
                "   ThreadId INTEGER, "
                "   AppId INTEGER, "
                "   Name TEXT, "
                "   PRIMARY KEY(ThreadId, AppId)"
                ")");

            //Create table for the Marks
            SQLite::Statement initMarkCommand(*database_,
                "CREATE TABLE marks ( "
                "   MarkId INTEGER PRIMARY KEY AUTOINCREMENT, "
                "   AppId INTEGER NOT NULL, "
                "   Name TEXT, "
                "   Timestamp INTEGER "
                ")");


            //Create table for the Plots
            SQLite::Statement initPlotCommand(*database_,
                "CREATE TABLE plots ( "
                "   PlotId INTEGER PRIMARY KEY AUTOINCREMENT, "
                "   AppId INTEGER NOT NULL, "
                "   Name TEXT, "
                "   Value REAL, "
                "   Timestamp INTEGER "
                ")");

            //Create table for the activities
            //We do not need to use AUTO_INCREMENT for these activities because their IDs are defined previously on user side
            //Same goes for ThreadId and AppId
            SQLite::Statement initActCommand(*database_,
                "CREATE TABLE activities ( "
                "   ActivityId INTEGER NOT NULL, "
                "   ThreadId INTEGER NOT NULL, "
                "   AppId INTEGER NOT NULL, "
                "   ParentId INTEGER, "
                "   Starttime INTEGER, "
                "   Stoptime INTEGER, "
                "   Name TEXT, "
                "   PRIMARY KEY(ActivityId, ThreadId, AppId)"
                ")");

            initAppInfoCommand.exec();
            initThreadInfoCommand.exec();
            initActCommand.exec();
            initMarkCommand.exec();
            initPlotCommand.exec();
        }
        catch (std::exception& error)
        {
            //If something went wrong during initialization of the database we should fail hard and early
            std::cout << __FUNCTION__ << ": " << error.what() << std::endl;
            exit(1);
        }

        //commit the transaction (or it will rollback)
        transaction.commit();
    }

    void DataStorage::storeResult(const CxxProf::ProfileObjects& objects)
    {
        if (!database_)
        {
            std::cout << "ERROR: No database loaded..." << std::endl;
            return;
        }

        //create a deferred transaction for the following commands
        //Simple explanation: If we do not wrap everything into a transaction here the performance would be VERY bad
        SQLite::Transaction transaction(*database_);

        //Add/Update the AppInfo
        unsigned int appId = updateAppInfo(objects.Info);
        updateThreadInfo(objects.Info, appId);

        //Add the Marks
        updateMarks(objects.Marks, appId);

        //Add the Plots
        updatePlots(objects.Plots, appId);

        //Add the ActivityResults to the Database
        updateActivities(objects.ActivityResults, appId);

        //commit the transaction (or it will rollback)
        transaction.commit();
    }

    unsigned int DataStorage::updateAppInfo(const CxxProf::AppInfo& info)
    {
        try
        {
            SQLite::Statement insertCommand(*database_,
                "INSERT INTO appinfo ( "
                "Name, "
                "Starttime) "
                "VALUES ( "
                "?, " //Name
                "? " //Timestamp
                ");");

            insertCommand.bind(1, info.Name);
            insertCommand.bind(2, info.Starttime);

            //We do not use the error-code here, but it is good to know that there is one if we need it
            int errorNum = insertCommand.exec();
        }
        catch (std::exception&)
            //catch (sqlite::sqlite_error& error)
        {
            //NOTE: Unfortunately we're getting contraint_errors by design here. This is
            //      because of the UNIQUEness of the 'Name' attribute.
            //      sqlite3cc does not return the expected error code, nor is there a
            //      way to distinguish sqlite_error exceptions from each other.
            //      So we have to ignore everything on purpose to not confuse the user...

            //std::cout << __FUNCTION__ << ": " << error.what() << std::endl;
        }

        unsigned int appId = 0;
        SQLite::Statement idQuery(*database_, "SELECT AppId FROM appinfo WHERE Name='" + info.Name + "';");
        while (idQuery.executeStep())
        {
            appId = idQuery.getColumn(0);
        }

        return appId;
    }
    void DataStorage::updateMarks(const std::vector<CxxProf::Mark>& marks, unsigned int appId)
    {
        std::vector<CxxProf::Mark>::const_iterator markIter = marks.begin();
        for (; markIter != marks.end(); ++markIter)
        {
            try
            {
                SQLite::Statement insertCommand(*database_,
                    "INSERT INTO marks ( "
                    "AppId, "
                    "Name, "
                    "Timestamp) "
                    "VALUES ( "
                    "?, " //AppId
                    "?, " //Name
                    "? " //Timestamp
                    ");");

                insertCommand.bind(1, appId);
                insertCommand.bind(2, markIter->Name);
                insertCommand.bind(3, markIter->Timestamp);

                //We do not use the error-code here, but it is good to know that there is one if we need it
                int error = insertCommand.exec();
            }
            catch (std::exception& error)
            {
                //TODO: How could we react better on errors? What errors are possible?
                std::cout << __FUNCTION__ << ": " << error.what() << std::endl;
            }
        }
    }
    void DataStorage::updatePlots(const std::vector<CxxProf::Plot>& plots, unsigned int appId)
    {
        std::vector<CxxProf::Plot>::const_iterator plotIter = plots.begin();
        for (; plotIter != plots.end(); ++plotIter)
        {
            SQLite::Statement insertCommand(*database_,
                "INSERT INTO plots ("
                "AppId, "
                "Name, "
                "Value, "
                "Timestamp) "
                "VALUES ( "
                "?, " //AppId
                "?, " //Name
                "?, " //Value
                "? " //Timestamp
                ");");

            insertCommand.bind(1, appId);
            insertCommand.bind(2, plotIter->Name);
            insertCommand.bind(3, plotIter->Value);
            insertCommand.bind(4, plotIter->Timestamp);

            try
            {
                //We do not use the error-code here, but it is good to know that there is one if we need it
                int error = insertCommand.exec();
            }
            catch (std::exception& error)
            {
                //TODO: How could we react better on errors? What errors are possible?
                std::cout << __FUNCTION__ << ": " << error.what() << std::endl;
            }
        }
    }
    void DataStorage::updateActivities(const std::vector<CxxProf::ActivityResult>& activities, unsigned int appId)
    {
        std::vector<CxxProf::ActivityResult>::const_iterator actIter = activities.begin();
        for (; actIter != activities.end(); ++actIter)
        {
            SQLite::Statement insertCommand(*database_,
                "INSERT INTO activities ("
                "ActivityId,"
                "ThreadId,"
                "AppId, "
                "ParentId,"
                "Starttime,"
                "Stoptime,"
                "Name) "
                "VALUES ( "
                "?, " //ActivityId
                "?, " //ThreadId
                "?, " //AppId
                "?, " //ParentId
                "?, " //Starttime
                "?, " //Stoptime
                "? " //Name 
                ");");

            insertCommand.bind(1, actIter->ActId);
            insertCommand.bind(2, actIter->ThreadId);
            insertCommand.bind(3, appId);
            insertCommand.bind(4, actIter->ParentId);
            insertCommand.bind(5, actIter->Starttime);
            insertCommand.bind(6, actIter->Stoptime);
            insertCommand.bind(7, actIter->Name);

            try
            {
                //We do not use the error-code here, but it is good to know that there is one if we need it
                int error = insertCommand.exec();
            }
            catch (std::exception& error)
            {
                //TODO: How could we react better on errors? What errors are possible?
                std::cout << __FUNCTION__ << ": " << error.what() << std::endl;
            }
        }
    }
    void DataStorage::updateThreadInfo(const CxxProf::AppInfo& info, unsigned int appId)
    {
        std::map<unsigned int, std::string>::const_iterator threadIter = info.ThreadAliases.begin();
        for (; threadIter != info.ThreadAliases.end(); ++threadIter)
        {
            try
            {
                SQLite::Statement insertCommand(*database_,
                    "INSERT INTO threadinfo ( "
                    "ThreadId, "
                    "AppId, "
                    "Name) "
                    "VALUES ( "
                    "?, " //ThreadId
                    "?, " //AppId
                    "? " //Name
                    ");");

                insertCommand.bind(1, threadIter->first);
                insertCommand.bind(2, appId);
                insertCommand.bind(3, threadIter->second);

                //We do not use the error-code here, but it is good to know that there is one if we need it
                int error = insertCommand.exec();
            }
            catch (std::exception&)
            //catch (sqlite::sqlite_error& error)
            {
                //NOTE: Unfortunately we're getting contraint_errors by design here. This is
                //      because of the UNIQUEness of the 'Name' attribute.
                //      sqlite3cc does not return the expected error code, nor is there a
                //      way to distinguish sqlite_error exceptions from each other.
                //      So we have to ignore everything on purpose to not confuse the user...

                //std::cout << __FUNCTION__ << ": " << error.what() << std::endl;
            }
        }
    }
} //namespace CxxProf
