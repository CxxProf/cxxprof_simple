
#pragma once

#include <cstdint>
#include <string>

namespace CxxProf
{
    struct Mark
    {
        std::string Name;
        std::int64_t Timestamp;

        Mark() :
            Name(""),
            Timestamp(0)
        {}
    };

}
