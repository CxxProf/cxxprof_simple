#pragma once

#include <cxxprof_simple/ProfileObjects.h>
#include <SQLiteCpp/SQLiteCpp.h>
#include <memory>

namespace CxxProf
{
    class DataStorage
    {
    public:
        /**
         * Creates a new database and stores the needed tables in it
         *  * Activities
         *  * Marks
         *  * Plots
         */
        DataStorage();
        virtual ~DataStorage();

        void initialize();
        
        /**
         * Stores the given objects into the database
         */
        void storeResult(const CxxProf::ProfileObjects& objects);

    private:
        /**
         * Initializes a new database with the given name for
         *  * Activities
         *  * Marks
         *  * Plots
         */
        void initDatabase(const std::string& filename);
        /**
         * This function returns the current DateStr in the following form:
         * DDMonYYYY_HHMMSS -> 31Apr2014_235959
         */
        std::string getCurrentDateStr();
        /**
        * Checks if the AppInfo has been added yet, then adds it if needed
        * Returns: The AppId which is given for this app
        */
        unsigned int updateAppInfo(const CxxProf::AppInfo& info);
        /**
        * Adds the marks to the database
        */
        void updateMarks(const std::vector<CxxProf::Mark>& marks, unsigned int appId);
        /**
        * Adds the plots to the database
        */
        void updatePlots(const std::vector<CxxProf::Plot>& plots, unsigned int appId);
        /**
         * Adds the activities to the database
         */
        void updateActivities(const std::vector<CxxProf::ActivityResult>& activities, unsigned int appId);
        /**
         * Checks if the ThreadInfo has been added yet, adds and updates it if needed
         */
        void updateThreadInfo(const CxxProf::AppInfo& info, unsigned int appId);

        /**
         * This is the connection to the database
         */
        std::shared_ptr<SQLite::Database> database_;

    };
} //namespace CxxProf
