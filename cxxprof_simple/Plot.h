
#pragma once

#include <cstdint>
#include <string>

namespace CxxProf
{
    struct Plot
    {
        std::string Name;
        std::int64_t Timestamp;
        double Value;

        Plot() :
            Name(""),
            Timestamp(0),
            Value(0.0)
        {}
    };

}
