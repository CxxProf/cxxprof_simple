
#pragma once

#ifdef WIN32
	#ifdef CXXPROF_SIMPLE
	    #define CxxProf_Simple_EXPORT __declspec( dllexport )
	#else
	    #define CxxProf_Simple_EXPORT __declspec( dllimport )
	#endif //CXXPROF_SIMPLE
#else
	#define CxxProf_Simple_EXPORT 
#endif
