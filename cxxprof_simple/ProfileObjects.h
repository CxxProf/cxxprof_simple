
#pragma once

#include "cxxprof_simple/AppInfo.h"
#include "cxxprof_simple/Mark.h"
#include "cxxprof_simple/Plot.h"
#include "cxxprof_simple/ActivityResult.h"
#include <vector>

namespace CxxProf
{

    struct ProfileObjects
    {
        AppInfo Info;
        std::vector<Mark> Marks;
        std::vector<Plot> Plots;
        std::vector<ActivityResult> ActivityResults;

        void clear()
        {
            Marks.clear();
            Plots.clear();
            ActivityResults.clear();
        }

        unsigned int size() const
        {
            return Marks.size() + Plots.size() + ActivityResults.size();
        }
        unsigned int empty() const
        {
            return Marks.empty() || Plots.empty() || ActivityResults.empty();
        }
    };

}
