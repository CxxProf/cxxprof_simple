
#pragma once

#include <cstdint>
#include <stdint.h>
#include <string>

namespace CxxProf
{

    struct ActivityResult
    {
        std::string Name;
        unsigned int ThreadId;
        unsigned int ActId;
        unsigned int ParentId;
        std::int64_t Starttime;
        std::int64_t Stoptime;

        ActivityResult() :
            Name(""),
            ThreadId(0),
            ActId(0),
            ParentId(0),
            Starttime(0),
            Stoptime(0)
        {}
    };

}
