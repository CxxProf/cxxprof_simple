
#pragma once

#include "cxxprof_simple/ActivityResult.h"
#include "cxxprof_simple/IActivity.h"

#include <chrono>
#include <cstdint>
#include <functional>
#include <stdint.h>
#include <string>

namespace CxxProf
{
    class Activity : public IActivity
    {
    public:
        typedef std::function<void(const ActivityResult&)> ResultCallback;

    public:
        Activity();
        Activity(const std::string& name, unsigned int actId, unsigned int threadId, unsigned int parentId, const std::chrono::high_resolution_clock::time_point& profilingStart, ResultCallback callback);
        virtual ~Activity();

        void setName(const std::string& name);
        void setThreadId(unsigned int threadId);
        void setActId(unsigned int actId);
        void setParentId(unsigned int parentId);
        void setResultCallback(ResultCallback callback);
        void setProfilingStart(const std::chrono::high_resolution_clock::time_point& profilingStart);

        void start();
        void shutdown();

    private:
        void executeCallback();

        std::string name_;
        unsigned int threadId_;
        unsigned int actId_;
        unsigned int parentId_;
        ResultCallback callback_;
        std::int64_t starttime_;
        std::chrono::high_resolution_clock::time_point profilingStart_;

        std::int64_t getCurrentTime();
    };

}
