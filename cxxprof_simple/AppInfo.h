
#pragma once

#include <cstdint>
#include <map>
#include <string>

namespace CxxProf
{

    struct AppInfo
    {
        std::string Name;
        std::int64_t Starttime;
        std::map<unsigned int, std::string> ThreadAliases;

        AppInfo() :
            Name(""),
            Starttime(0)
        {}
    };

}
