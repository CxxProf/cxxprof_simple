from conans import ConanFile, CMake

class CxxProfConan(ConanFile):
    name = "cxxprof_simple"
    version = "1.0.0"
    url = "https://gitlab.com/groups/CxxProf"
    license = "GNU LESSER GENERAL PUBLIC LICENSE Version 3"
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    exports = "*"

    def config(self):
        self.requires.add("sqlite3/3.27.2@bincrafters/stable")
        self.requires.add("sqlitecpp/2.3.0@bincrafters/stable")
        
    def build(self):
        cmake = CMake(self)
        cmake.definitions['CMAKE_EXPORT_COMPILE_COMMANDS'] = True
        cmake.configure()
        cmake.build()
        cmake.install()

    def package(self):
        self.copy("*.h", dst="include/cxxprof_simple", src="%s/cxxprof_simple" % self.source_folder)
        self.copy("*.lib", dst="lib", src="lib")
        self.copy("*.dll", dst="bin", src="bin")
        self.copy("*.so", dst="bin", src="bin")
        self.copy("*.a", dst="lib", src="lib")

    def package_info(self):
        self.cpp_info.libs = ["cxxprof_simple"]
        